import React, { Component } from 'react'
import validator from 'validator';

export class Form extends Component {
    constructor(props) {
        super(props)

        this.state = {
            fname: null,
            lname: null,
            age: null,
            email: null,
            password: null,
            gender: null,
            role: null,
            repeatPassword: null,
            condition: false,
            success : {
                display:'none'
            }
        }
    }

    OnChange = (event) => {
        this.setState({
            // fname: event.target.value,
            // lname: event.target.value,
            // age: event.target.value,
            // gender: event.target.value,
            // role: event.target.value,
            // email : event.target.email,
            // password : event.target.password,
            // repeatPassword:event.target.repeatPassword,
            // condition: event.target.condition
        })
    }
    buttonSubmit = (event) => {
        event.preventDefault()
        let flag = true;
        const firstName = event.target.fname
        const lastName = event.target.lname
        const currentAge = event.target.age
        const genderType = event.target.gender
        const position = event.target.role
        const userEmail = event.target.email
        const userPassword = event.target.password
        const RepeatedPassWord = event.target.repeatPassword
        const Condition = event.target.repeatPassword
        if (!validator.isAlpha(firstName.value)) {
            flag = false;
            this.setState({
                fname: ''
            })
        } else {
            this.setState({
                fname: event.target.fname.value
            })
        }

        if (!validator.isAlpha(lastName.value)) {
            flag = false;
            this.setState({
                lname: ''
            })
        } else {
            this.setState({
                lname: event.target.lname.value
            })
        }

        if (!validator.isInt(currentAge.value)) {
            flag = false;
            this.setState({
                age: ''
            })
        } else {
            this.setState({
                age: event.target.age.value
            })
        }

        if (genderType.value === 'Select') {
            flag = false;
            this.setState({
                gender: ''
            })
        } else {
            this.setState({
                gender: event.target.gender.value
            })
        }

        if(position.value === "Select") {
            flag = false;
            this.setState({
                role : ''
            })
        } else {
            this.setState({
                role : event.target.role.value
            })
        }

        if(!validator.isEmail(userEmail.value)){
            flag = false;
            this.setState({
                email : ''
            })
        } else {
            this.setState({
                email : event.target.email.value
            })
        }
        if(!validator.isStrongPassword(userPassword.value)){
            flag = false;
            this.setState({
                password : ''
            })
        } else {
            this.setState({
                password : event.target.password.value
            })
        }

        if(userPassword!==RepeatedPassWord){
            flag = false;
            this.setState({
                repeatPassword   : ''
            })
        } else {
            this.setState({
                repeatPassword : event.target.repeatPassword.value
            })
        }
        if(Condition.checked===false){
            flag = false;
            this.setState({
                condition   : ''
            })
        } else {
            this.setState({
                condition : event.target.condition.value
            })
        }
        if(flag===true){
            this.setState({
                success : {
                    display : 'none'
                }
            })
        }
    }

    render() {
        const style = {
            display: 'block'
        }
        return (
            <div>
                <form onSubmit={this.buttonSubmit} className='form' >
                    <div className='firstName'> 
                    <label> First Name </label>
                    <input type='text' label='first-name' id='fname' name='fname' onChange={this.OnChange} value={this.state.fname} />
                    <p className='fname-warning' id='fnameWarning' style={this.state.fname !== '' ? {} : style}> please enter valid first name</p>
                    <i class="fa-solid fa-font"></i>
                    </div>
                    <label> Last Name </label>
                    <input type='text' id='lname' onChange={this.OnChange} />
                    <p className='lname-warning' id='lnameWarning' style={this.state.lname !== '' ? {} : style}> please enter valid last name</p>
                    <label> Age </label>
                    <input type='text' id='age' onChange={this.OnChange} />
                    <p className='age-warning' id='ageWarning' style={this.state.age !== '' ? {} : style}>  please enter valid age</p>
                    <label> Gender </label>
                    <select id='gender' onChange={this.OnChange}>
                        <option> Select</option>
                        <option> Male </option>
                        <option> Female </option>
                        <option> Other</option>
                    </select>
                    <p className='gender-warning' id='genderWarning' style={this.state.gender !== '' ? {} : style}> please select gender</p>

                    <label> Role</label>
                    <select id='role' onChange={this.OnChange}>
                        <option> Select</option>
                        <option> Developer </option>
                        <option> Senior Developer </option>
                        <option> Lead Engineer </option>
                        <option> CTO </option>
                        <option> Other </option>
                    </select>
                    <p className='role-warning' id='roleWarning' style={this.state.role !== '' ? {} : style}> please select Position </p>
                    <label> Email </label>
                    <input type='text' id='email' onChange={this.OnChange}></input>
                    <p className='email-warning' id='emailWarning' style={this.state.email !== '' ? {} : style}> please enter valid email id</p>
                    <label> Password</label>
                    <input type='password' id='password' onChange={this.OnChange}></input>
                    <span className='passwordrules'> Password must contain : length atleast 8, 1 uppercase 1 ,lowercase 1 ,number 1 ,special charecter</span>
                    <p className='email-warning' id='passwordWarning' style={this.state.password !== '' ? {} : style}> please enter valid password</p>
                    <label> Repeat Password</label>
                    <input type='password' id='repeatPassword' onChange={this.OnChange}></input>
                    <p className='email-warning' id='repasswordWarning' style={this.state.repeatPassword !== '' ? {} : style}> password must be same</p>
                    <input type='checkbox' id='checkbox' onChange={this.OnChange}></input>
                    <label id='condition'> agree with our terms and condition</label>
                    <p className='email-warning' id='condition' style={this.state.condition !== '' ? {} : style}> please agree with our terms</p>
                    <button className='submit-button' type='submit'> Submit </button>
                    </form>
                    {/* <h1 className={this.state.success}> Sign in Successfull </h1> */}
            </div>
        )
    }
}

export default Form