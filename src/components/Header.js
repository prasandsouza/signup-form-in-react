import React, { Component } from 'react'
import logo from '../images/preview.jpg'
import '../style.css'
export class Header extends Component {
  render() {
    return (
      <div className='header-component'>
        <img src={logo} className="logo" alt='logo'/>
        <ul>
            <li>Home </li>
            <li> Setting </li>
            <li> About </li>
            <li> Contact</li>
        </ul>
        {/* <h3> home</h3>
        <h3> setting </h3>
        <h3> about</h3>
        <h3> contact</h3> */}
      </div>
    )
  }
}

export default Header